var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('pages/features', { title: 'Features Page Title' });
});

module.exports = router;
